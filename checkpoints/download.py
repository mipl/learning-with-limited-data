from __future__ import print_function
import pickle
import os
import numpy as np

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


DL_BLACK_LIST = ['dl-01', 'dl-12', 'dl-13', 'dl-15']

ROOT_PATH='./checkpoints'
HOST_PATH='/work/mrodriguez/limited-data-checkpoints'

TOKEN_FILENAME='{}/token.pickle'.format(ROOT_PATH)
CREDENTIAL_FILENAME='{}/credentials.json'.format(ROOT_PATH)


EXPERIMENT_NAME=['auto-ablation', 'auto-cross-kfold']
EXP_FOLDER_NAME=['auto_ablation', 'cross_kfold_ablation']


SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = '1GECoXg8hU2V1VIvbGdqcZDzcsi16zynRq56L72F6xkM'
# exp_name = EXPERIMENT_NAME[0]

datasets = ["CK", "MMI", "OULU", "MUG", "AFEW"]
models = ["VGG16-lstm", "Inceptionv3-lstm", "ResNet18-lstm", "ResNet101-lstm", "C3D", "c3d-block-lstm", "I3D", "ResNet3D-18", "ResNet3D-101"]

def tprint(text, n_tabs=0):
    tabs = "\t" * n_tabs
    print("{}{}".format(tabs, text))


EXP_ORDER = {
    0: str(np.array([0,0,0,0])),
    1: str(np.array([0,0,1,0])),
    2: str(np.array([0,0,0,1])),
    3: str(np.array([1,0,0,0])),
    4: str(np.array([1,0,1,0])),
    5: str(np.array([1,0,0,1])),
}

def extract_results(model_name, sheet, ix):
    tprint("extracting: {}".format(model_name))
    shift = 2
    step = 3

    dataset_map = {}
    results = {}

    for i in range(len(sheet[ix+1])):
        if sheet[ix+1][i] in datasets:
            dataset_map[sheet[ix+1][i]] = i

    for row in range(ix+shift, ix+shift + step*8, step):
        config = str(np.array([int(c) for c in sheet[row][1:5]]))

        if config not in EXP_ORDER.values():
            continue


        # tprint(config, 1)
        for db_name, col in dataset_map.items():
            # tprint(db_name, 2)
            if db_name not in results:
                results[db_name] = {}

            # if config not in results[db_name]:
            #     results[db_name][config] =

            if col < len(sheet[row]):
                checkpoint = sheet[row+2][col]
                # if np.abs(acc - np.mean(acc_list)) > 0.01:
                #     tprint("row: {}, col: {}".format(row+1, chr(col+65)), 3)
                #     tprint(acc, 3)
                #     tprint(acc_list, 3)
                #     tprint(np.abs(acc - np.mean(acc_list)), 4)
                results[db_name][config] = checkpoint

    return results

def config2name(config):
    str = ""
    config = [int(n) for n in config.split("[")[-1].split("]")[0].split(' ')]
    # print(config)
    config = np.array(config)

    # print(config)
    # print(config.shape)
    if config[0] == 0:
        str += "ri"
    else:
        str += "ft"

    if config[2] == 1:
        str += "_caug"

    if config[3] == 1:
        str += "_syn"

    return str

def main(exp_name, exp_folder_name):
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file ./tables/token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(TOKEN_FILENAME):
        with open(TOKEN_FILENAME, 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                CREDENTIAL_FILENAME, SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open(TOKEN_FILENAME, 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                range=exp_name).execute()
    values = result.get('values', [])

    if not values:
        print('No data found.')
    else:
        results = {}
        for i, row in enumerate(values):
            if len(row) == 1 and row[0] in models:
                model_name = row[0]
                # print(i, row)
                results[model_name] = extract_results(model_name, values, i)


        dl_map = {}
        dl_blacklist = []

        for model in models:
            for db_name in datasets:
                for config in list(EXP_ORDER.values()):
                    checkpoint = results[model][db_name][config]

                    if len(checkpoint.split(' ')) == 2:
                        cp_path, cp_machine = checkpoint.split(' ')
                        cp_machine = cp_machine.lower()

                        if len(cp_machine.split('-')) == 1:
                            #dlxx case
                            cp_machine = "dl-{}".format(cp_machine.split('dl')[-1])

                        if cp_machine not in dl_map:
                            dl_map[cp_machine] = []
                        if (cp_machine, cp_path) not in dl_blacklist:
                            if exp_name == "auto-cross-kfold":
                                dl_map[cp_machine].append((cp_path, config, model))
                            else:
                                dl_map[cp_machine].append((cp_path, config, model, db_name))

                            dl_blacklist.append((cp_machine, cp_path))

        with open("./checkpoints/dls/script-{}.sh".format(exp_name), "w") as super_file:
            for dl, cp_list in dl_map.items():
                if dl not in DL_BLACK_LIST:
                    filename = "./checkpoints/dls/{}/{}.sh".format(exp_name, dl)
                    super_file.write("sh {}\n".format(filename))
                    with open(filename, 'w') as file:
                        for e in cp_list:
                            if exp_name == "auto-cross-kfold":
                                (cp_path, config, model) = e
                                db_name = "CK-MMI-OULU-MUG"
                            else:
                                (cp_path, config, model, db_name) = e
                                # print(config2name(config))

                            file_path = "{}/{}/{}/{}/{}".format(HOST_PATH, exp_name, model, db_name, config2name(config))

                            file.write("mkdir -p {}\n".format(file_path))

                            # dl_ = "".join(dl.split("-"))

                            cp_path = cp_path.split("../metadata/")[-1].split("/logs")[0]
                            cp_path = "/work/mrodriguez/metadata/fer/{}/checkpoints".format(cp_path)
                            # print(cp_path)

                            file.write("scp -r  {}:{} {}\n".format(dl,cp_path,file_path))



if __name__ == '__main__':
    for exp_name, exp_folder_name in zip(EXPERIMENT_NAME, EXP_FOLDER_NAME):
        if exp_name == 'auto-cross-kfold':
            datasets.remove('AFEW')
        main(exp_name, exp_folder_name)
