from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import numpy as np
import ast

DL_BLACK_LIST = ['dl-01', 'dl-12', 'dl-13', 'dl-15']

ROOT_PATH='./checkpoints'
HOST_PATH='/work/mrodriguez/limited-data-checkpoints'

TOKEN_FILENAME='{}/token.pickle'.format(ROOT_PATH)
CREDENTIAL_FILENAME='{}/credentials.json'.format(ROOT_PATH)


EXPERIMENT_NAME=['auto-ablation', 'auto-cross-kfold']
EXP_FOLDER_NAME=['auto_ablation', 'cross_kfold_ablation']


SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly']

# The ID and range of a sample spreadsheet.
SAMPLE_SPREADSHEET_ID = '1GECoXg8hU2V1VIvbGdqcZDzcsi16zynRq56L72F6xkM'
# exp_name = EXPERIMENT_NAME[0]

datasets = ["CK", "MMI", "OULU", "MUG", "AFEW"]
models = ["VGG16-lstm", "Inceptionv3-lstm", "ResNet18-lstm", "ResNet101-lstm", "C3D", "c3d-block-lstm", "I3D", "ResNet3D-18", "ResNet3D-101"]


exp_order = {
    0: str(np.array([1,0,0,0])),
    1: str(np.array([1,0,1,0])),
    2: str(np.array([1,0,0,1])),
    3: str(np.array([1,0,1,1]))
}

def create_dir(path):
    if not os.path.exists(path):
        while True:
            try:
                os.makedirs(path)
                break
            except:
                continue

def proc_acc(acc):
    if "dl" in acc or "DL" in acc or "" == acc:
        return ACC_NO_EXIST

    acc = float(acc.replace(",", "."))
    if acc > 1:
        acc /= 100
    return acc

db_step_map = {
    "CK":0, "MMI":1, "OULU":2, "MUG":3, "AFEW":4,
}

def extract_results(model_name, sheet, row):
    print("extracting:", model_name)
    shift = 4
    step = 7
    n_configs = 4
    model_map = {}
    for col in range(shift, shift + step * n_configs, step):
        config = np.array([int(c) for c in sheet[row][col:col+shift]])
        config_s = str(config)
        # print(config)
        if config_s not in model_map:
            model_map[config_s] = {}

        for trdb_row in range(row+2, row+7):
            trdb_name = sheet[trdb_row][col-1]

            if trdb_name in datasets:
                if trdb_name not in model_map:
                    model_map[config_s][trdb_name] = {}

                col_step = db_step_map[trdb_name]
                if len(sheet[trdb_row]) >= col+col_step:
                    checkpoint = sheet[trdb_row][col+col_step]

                    model_map[config_s][trdb_name] = checkpoint

    return model_map

# def conv_s(s):
#     return s.lower().replace('-','').replace('_','')


def config2name(config):
    str = ""
    config = [int(n) for n in config.split("[")[-1].split("]")[0].split(' ')]
    # print(config)
    config = np.array(config)

    # print(config)
    # print(config.shape)
    if config[0] == 0:
        str += "ri"
    else:
        str += "ft"

    if config[2] == 1:
        str += "_caug"

    if config[3] == 1:
        str += "_syn"

    return str

def main():
    """Shows basic usage of the Sheets API.
    Prints values from a sample spreadsheet.
    """
    creds = None
    # The file ./tables/token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(TOKEN_FILENAME):
        with open(TOKEN_FILENAME, 'rb') as token:
            creds = pickle.load(token)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                CREDENTIAL_FILENAME, SCOPES)
            creds = flow.run_local_server()
        # Save the credentials for the next run
        with open(TOKEN_FILENAME, 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    # Call the Sheets API
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=SAMPLE_SPREADSHEET_ID,
                                range='auto-1-vs-all').execute()
    values = result.get('values', [])

    if not values:
        print('No data found.')
    else:
        results = {}
        for i, row in enumerate(values):
            if len(row) > 1:
                if row[1] in models:
                    results[row[1]] = extract_results(row[1], values, i)


        dl_map = {}
        dl_blacklist = []

        for model in models:
            for config in list(exp_order.values()):
                for db_name in datasets:
                    checkpoint = results[model][config][db_name]

                    if type(checkpoint) != type("s"):
                        continue

                    if len(checkpoint.split(' ')) == 2:
                        cp_path, cp_machine = checkpoint.split(' ')
                        cp_machine = cp_machine.lower()

                        if cp_machine == "":
                            continue
                        cp_machine = cp_machine.split(".recod")[0]

                        if len(cp_machine.split('-')) == 1:
                            #dlxx case
                            cp_machine = "dl-{}".format(cp_machine.split('dl')[-1])

                        if cp_machine not in dl_map:
                            dl_map[cp_machine] = []

                        if (cp_machine, cp_path) not in dl_blacklist:
                            dl_map[cp_machine].append((cp_path, config, model, db_name))

                            dl_blacklist.append((cp_machine, cp_path))

        with open("./checkpoints/dls/script-auto-1-vs-all.sh", "w") as super_file:
            for dl, cp_list in dl_map.items():
                if dl not in DL_BLACK_LIST:
                    filename = "./checkpoints/dls/{}/{}.sh".format('auto-1-vs-all', dl)
                    super_file.write("sh {}\n".format(filename))
                    with open(filename, 'w') as file:
                        for (cp_path, config, model, db_name) in cp_list:
                            file_path = "{}/{}/{}/{}/{}".format(HOST_PATH, 'auto-1-vs-all', model, db_name, config2name(config))


                            file.write("mkdir -p {}\n".format(file_path))

                            # dl_ = "".join(dl.split("-"))

                            cp_path = cp_path.split("../metadata/")[-1].split("/logs")[0]
                            cp_path = "/work/mrodriguez/metadata/fer/{}/checkpoints".format(cp_path)
                            # print(cp_path)

                            file.write("scp -r  {}:{} {}\n".format(dl,cp_path,file_path))



if __name__ == '__main__':
    main()
