mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/VGG16-lstm/CK-MMI-OULU-MUG/ri_caug
scp -r  dl-17:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/vgg16_lstm/2019-08-19/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/VGG16-lstm/CK-MMI-OULU-MUG/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/VGG16-lstm/CK-MMI-OULU-MUG/ft
scp -r  dl-17:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/vgg16_lstm/2019-08-17/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/VGG16-lstm/CK-MMI-OULU-MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/Inceptionv3-lstm/CK-MMI-OULU-MUG/ri
scp -r  dl-17:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/inceptionv3_lstm/2019-08-21/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/Inceptionv3-lstm/CK-MMI-OULU-MUG/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/Inceptionv3-lstm/CK-MMI-OULU-MUG/ri_caug
scp -r  dl-17:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/inceptionv3_lstm/2019-08-22/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/Inceptionv3-lstm/CK-MMI-OULU-MUG/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/Inceptionv3-lstm/CK-MMI-OULU-MUG/ft
scp -r  dl-17:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/inceptionv3_lstm/2019-08-17/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/Inceptionv3-lstm/CK-MMI-OULU-MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/Inceptionv3-lstm/CK-MMI-OULU-MUG/ft_caug
scp -r  dl-17:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/inceptionv3_lstm/2019-08-21/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/Inceptionv3-lstm/CK-MMI-OULU-MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet101-lstm/CK-MMI-OULU-MUG/ft_syn
scp -r  dl-17:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/resnet101_lstm/2019-08-25/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet101-lstm/CK-MMI-OULU-MUG/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/c3d-block-lstm/CK-MMI-OULU-MUG/ri_syn
scp -r  dl-17:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/c3d_block_lstm/2019-08-19/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/c3d-block-lstm/CK-MMI-OULU-MUG/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/I3D/CK-MMI-OULU-MUG/ri_syn
scp -r  dl-17:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/i3d/2019-08-24/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/I3D/CK-MMI-OULU-MUG/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet3D-18/CK-MMI-OULU-MUG/ri
scp -r  dl-17:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/resnet3d_18/2019-08-23/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet3D-18/CK-MMI-OULU-MUG/ri
