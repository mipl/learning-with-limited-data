mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet18-lstm/CK-MMI-OULU-MUG/ri
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/resnet18_lstm/2019-08-25/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet18-lstm/CK-MMI-OULU-MUG/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet18-lstm/CK-MMI-OULU-MUG/ft_caug
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/resnet18_lstm/2019-08-25/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet18-lstm/CK-MMI-OULU-MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/C3D/CK-MMI-OULU-MUG/ri
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/c3d/2019-08-21/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/C3D/CK-MMI-OULU-MUG/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/C3D/CK-MMI-OULU-MUG/ri_caug
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/c3d/2019-08-22/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/C3D/CK-MMI-OULU-MUG/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/C3D/CK-MMI-OULU-MUG/ri_syn
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/c3d/2019-08-21/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/C3D/CK-MMI-OULU-MUG/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/C3D/CK-MMI-OULU-MUG/ft
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/c3d/2019-08-17/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/C3D/CK-MMI-OULU-MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/C3D/CK-MMI-OULU-MUG/ft_caug
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/c3d/2019-08-18/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/C3D/CK-MMI-OULU-MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/c3d-block-lstm/CK-MMI-OULU-MUG/ri
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/c3d_block_lstm/2019-08-17/5/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/c3d-block-lstm/CK-MMI-OULU-MUG/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/c3d-block-lstm/CK-MMI-OULU-MUG/ri_caug
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/c3d_block_lstm/2019-08-19/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/c3d-block-lstm/CK-MMI-OULU-MUG/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/c3d-block-lstm/CK-MMI-OULU-MUG/ft
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/c3d_block_lstm/2019-08-17/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/c3d-block-lstm/CK-MMI-OULU-MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/c3d-block-lstm/CK-MMI-OULU-MUG/ft_caug
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/c3d_block_lstm/2019-08-17/9/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/c3d-block-lstm/CK-MMI-OULU-MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/c3d-block-lstm/CK-MMI-OULU-MUG/ft_syn
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/c3d_block_lstm/2019-08-17/10/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/c3d-block-lstm/CK-MMI-OULU-MUG/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet3D-18/CK-MMI-OULU-MUG/ft
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/resnet3d_18/2019-08-23/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet3D-18/CK-MMI-OULU-MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet3D-18/CK-MMI-OULU-MUG/ft_caug
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/resnet3d_18/2019-08-23/3/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet3D-18/CK-MMI-OULU-MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet3D-18/CK-MMI-OULU-MUG/ft_syn
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/resnet3d_18/2019-08-23/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet3D-18/CK-MMI-OULU-MUG/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet3D-101/CK-MMI-OULU-MUG/ri
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/resnet3d_101/2019-08-21/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet3D-101/CK-MMI-OULU-MUG/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet3D-101/CK-MMI-OULU-MUG/ri_syn
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK_MMI_OULU_MUG-experiments/resnet3d_101/2019-08-22/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-cross-kfold/ResNet3D-101/CK-MMI-OULU-MUG/ri_syn
