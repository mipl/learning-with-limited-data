mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/OULU/ri_syn
scp -r  dl-02:/work/mrodriguez/metadata/fer/OULU-experiments/vgg16_lstm/2020-01-21/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/OULU/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MMI/ri
scp -r  dl-02:/work/mrodriguez/metadata/fer/MMI-experiments/inceptionv3_lstm/2019-07-16/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MMI/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MMI/ri
scp -r  dl-02:/work/mrodriguez/metadata/fer/MMI-experiments/resnet18_lstm/2019-05-18/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MMI/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MMI/ri_syn
scp -r  dl-02:/work/mrodriguez/metadata/fer/MMI-experiments/resnet18_lstm/2019-07-14/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MMI/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/CK/ri
scp -r  dl-02:/work/mrodriguez/metadata/fer/CK-experiments/c3d/2019-02-17/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/CK/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/CK/ft
scp -r  dl-02:/work/mrodriguez/metadata/fer/CK-experiments/c3d/2019-02-17/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/CK/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MUG/ri_syn
scp -r  dl-02:/work/mrodriguez/metadata/fer/MUG-experiments/c3d/2019-07-05/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MUG/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/CK/ft
scp -r  dl-02:/work/mrodriguez/metadata/fer/CK-experiments/c3d_block_lstm/2019-02-18/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/CK/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MMI/ft_syn
scp -r  dl-02:/work/mrodriguez/metadata/fer/MMI-experiments/c3d_block_lstm/2019-07-15/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MMI/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/OULU/ri_syn
scp -r  dl-02:/work/mrodriguez/metadata/fer/OULU-experiments/c3d_block_lstm/2020-01-21/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/OULU/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MMI/ri_syn
scp -r  dl-02:/work/mrodriguez/metadata/fer/MMI-experiments/resnet3d_18/2019-07-15/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MMI/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MMI/ft_syn
scp -r  dl-02:/work/mrodriguez/metadata/fer/MMI-experiments/resnet3d_18/2019-07-15/3/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MMI/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/OULU/ri_syn
scp -r  dl-02:/work/mrodriguez/metadata/fer/OULU-experiments/resnet3d_18/2019-07-17/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/OULU/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/OULU/ft_syn
scp -r  dl-02:/work/mrodriguez/metadata/fer/OULU-experiments/resnet3d_18/2019-07-17/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/OULU/ft_syn
