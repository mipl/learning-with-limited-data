mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/MMI/ri_syn
scp -r  dl-04:/work/mrodriguez/metadata/fer/MMI-experiments/vgg16_lstm/2019-07-15/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/MMI/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/OULU/ri
scp -r  dl-04:/work/mrodriguez/metadata/fer/OULU-experiments/inceptionv3_lstm/2019-02-20/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/OULU/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MMI/ft_syn
scp -r  dl-04:/work/mrodriguez/metadata/fer/MMI-experiments/resnet18_lstm/2019-07-14/5/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MMI/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/AFEW/ri
scp -r  dl-04:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet18_lstm/2019-06-07/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/AFEW/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MMI/ri_syn
scp -r  dl-04:/work/mrodriguez/metadata/fer/MMI-experiments/resnet101_lstm/2019-07-23/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MMI/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/OULU/ft
scp -r  dl-04:/work/mrodriguez/metadata/fer/OULU-experiments/resnet101_lstm/2019-05-13/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/OULU/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MUG/ri
scp -r  dl-04:/work/mrodriguez/metadata/fer/MUG-experiments/resnet101_lstm/2019-05-28/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MUG/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MUG/ri_caug
scp -r  dl-04:/work/mrodriguez/metadata/fer/MUG-experiments/resnet101_lstm/2019-05-28/6/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MUG/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MMI/ri_syn
scp -r  dl-04:/work/mrodriguez/metadata/fer/MMI-experiments/c3d/2019-07-10/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MMI/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/OULU/ri_caug
scp -r  dl-04:/work/mrodriguez/metadata/fer/OULU-experiments/c3d/2020-01-21/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/OULU/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/AFEW/ft
scp -r  dl-04:/work/mrodriguez/metadata/fer/AFEW-experiments/c3d/2019-06-06/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/AFEW/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/OULU/ri
scp -r  dl-04:/work/mrodriguez/metadata/fer/OULU-experiments/i3d/2019-05-08/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/OULU/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/OULU/ri_caug
scp -r  dl-04:/work/mrodriguez/metadata/fer/OULU-experiments/i3d/2019-05-08/10/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/OULU/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/AFEW/ft
scp -r  dl-04:/work/mrodriguez/metadata/fer/AFEW-experiments/i3d/2019-06-07/6/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/AFEW/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/CK/ft_caug
scp -r  dl-04:/work/mrodriguez/metadata/fer/CK-experiments/resnet3d_18/2019-06-06/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/CK/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MMI/ri
scp -r  dl-04:/work/mrodriguez/metadata/fer/MMI-experiments/resnet3d_18/2019-06-06/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MMI/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/OULU/ri
scp -r  dl-04:/work/mrodriguez/metadata/fer/OULU-experiments/resnet3d_18/2019-06-06/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/OULU/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/OULU/ri_caug
scp -r  dl-04:/work/mrodriguez/metadata/fer/OULU-experiments/resnet3d_18/2019-06-06/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/OULU/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/OULU/ri_syn
scp -r  dl-04:/work/mrodriguez/metadata/fer/OULU-experiments/resnet3d_101/2019-07-26/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/OULU/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/OULU/ft_syn
scp -r  dl-04:/work/mrodriguez/metadata/fer/OULU-experiments/resnet3d_101/2019-07-27/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/OULU/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MUG/ri
scp -r  dl-04:/work/mrodriguez/metadata/fer/MUG-experiments/resnet3d_101/2019-05-26/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MUG/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MUG/ri_caug
scp -r  dl-04:/work/mrodriguez/metadata/fer/MUG-experiments/resnet3d_101/2019-05-29/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MUG/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/AFEW/ft
scp -r  dl-04:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet3d_101/2019-06-07/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/AFEW/ft
