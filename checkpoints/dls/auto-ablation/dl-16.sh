mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/AFEW/ri_syn
scp -r  dl-16:/work/mrodriguez/metadata/fer/AFEW-experiments/vgg16_lstm/2019-12-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/AFEW/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/CK/ri_syn
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK-experiments/c3d/2019-06-16/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/CK/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/CK/ft_syn
scp -r  dl-16:/work/mrodriguez/metadata/fer/CK-experiments/c3d/2019-06-16/3/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/CK/ft_syn
