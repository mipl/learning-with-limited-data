mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/AFEW/ri_caug
scp -r  dl-14:/work/mrodriguez/metadata/fer/AFEW-experiments/c3d/2019-06-06/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/AFEW/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/AFEW/ft_caug
scp -r  dl-14:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet3d_101/2019-06-07/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/AFEW/ft_caug
