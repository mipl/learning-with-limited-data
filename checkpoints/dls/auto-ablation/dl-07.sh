mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/MUG/ft_caug
scp -r  dl-07:/work/mrodriguez/metadata/fer/MUG-experiments/vgg16_lstm/2019-06-02/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/AFEW/ri
scp -r  dl-07:/work/mrodriguez/metadata/fer/AFEW-experiments/vgg16_lstm/2019-06-07/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/AFEW/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MMI/ri_caug
scp -r  dl-07:/work/mrodriguez/metadata/fer/MMI-experiments/inceptionv3_lstm/2019-02-20/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MMI/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/OULU/ri
scp -r  dl-07:/work/mrodriguez/metadata/fer/OULU-experiments/resnet18_lstm/2019-05-19/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/OULU/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/OULU/ft
scp -r  dl-07:/work/mrodriguez/metadata/fer/OULU-experiments/resnet18_lstm/2019-05-18/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/OULU/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/AFEW/ft_syn
scp -r  dl-07:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet18_lstm/2020-01-12/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/AFEW/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MMI/ri_caug
scp -r  dl-07:/work/mrodriguez/metadata/fer/MMI-experiments/c3d/2019-05-15/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MMI/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/OULU/ri_syn
scp -r  dl-07:/work/mrodriguez/metadata/fer/OULU-experiments/c3d/2020-01-21/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/OULU/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/OULU/ft_caug
scp -r  dl-07:/work/mrodriguez/metadata/fer/OULU-experiments/c3d/2019-02-17/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/OULU/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MUG/ft
scp -r  dl-07:/work/mrodriguez/metadata/fer/MUG-experiments/c3d/2019-05-29/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/AFEW/ft_caug
scp -r  dl-07:/work/mrodriguez/metadata/fer/AFEW-experiments/i3d/2019-06-07/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/AFEW/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MUG/ft
scp -r  dl-07:/work/mrodriguez/metadata/fer/MUG-experiments/resnet3d_18/2019-06-06/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MUG/ft
