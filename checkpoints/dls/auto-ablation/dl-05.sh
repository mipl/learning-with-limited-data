mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/CK/ri_syn
scp -r  dl-05:/work/mrodriguez/metadata/fer/CK-experiments/resnet18_lstm/2019-06-16/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/CK/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MMI/ft_caug
scp -r  dl-05:/work/mrodriguez/metadata/fer/MMI-experiments/resnet18_lstm/2019-05-22/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MMI/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/OULU/ri_caug
scp -r  dl-05:/work/mrodriguez/metadata/fer/OULU-experiments/resnet18_lstm/2019-05-18/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/OULU/ri_caug
