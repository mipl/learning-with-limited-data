mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MUG/ri_caug
scp -r  dl-03:/work/mrodriguez/metadata/fer/MUG-experiments/inceptionv3_lstm/2019-06-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MUG/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MMI/ri_caug
scp -r  dl-03:/work/mrodriguez/metadata/fer/MMI-experiments/resnet18_lstm/2019-05-19/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MMI/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MMI/ft
scp -r  dl-03:/work/mrodriguez/metadata/fer/MMI-experiments/resnet18_lstm/2019-05-18/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MMI/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MUG/ri
scp -r  dl-03:/work/mrodriguez/metadata/fer/MUG-experiments/resnet18_lstm/2019-05-22/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MUG/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MMI/ri_caug
scp -r  dl-03:/work/mrodriguez/metadata/fer/MMI-experiments/resnet101_lstm/2019-05-26/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MMI/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/CK/ri_caug
scp -r  dl-03:/work/mrodriguez/metadata/fer/CK-experiments/c3d/2019-08-15/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/CK/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MMI/ft
scp -r  dl-03:/work/mrodriguez/metadata/fer/MMI-experiments/c3d/2019-02-17/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MMI/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MMI/ft_caug
scp -r  dl-03:/work/mrodriguez/metadata/fer/MMI-experiments/c3d/2019-02-17/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MMI/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/OULU/ri
scp -r  dl-03:/work/mrodriguez/metadata/fer/OULU-experiments/c3d/2019-02-17/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/OULU/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/CK/ri
scp -r  dl-03:/work/mrodriguez/metadata/fer/CK-experiments/c3d_block_lstm/2019-08-15/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/CK/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MMI/ri_syn
scp -r  dl-03:/work/mrodriguez/metadata/fer/MMI-experiments/c3d_block_lstm/2019-07-15/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MMI/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MMI/ft
scp -r  dl-03:/work/mrodriguez/metadata/fer/MMI-experiments/c3d_block_lstm/2019-02-18/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MMI/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MUG/ri
scp -r  dl-03:/work/mrodriguez/metadata/fer/MUG-experiments/c3d_block_lstm/2019-06-04/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MUG/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MUG/ri_syn
scp -r  dl-03:/work/mrodriguez/metadata/fer/MUG-experiments/c3d_block_lstm/2019-08-09/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MUG/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/AFEW/ri
scp -r  dl-03:/work/mrodriguez/metadata/fer/AFEW-experiments/c3d_block_lstm/2019-06-06/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/AFEW/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/AFEW/ft_caug
scp -r  dl-03:/work/mrodriguez/metadata/fer/AFEW-experiments/c3d_block_lstm/2019-06-07/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/AFEW/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/AFEW/ri
scp -r  dl-03:/work/mrodriguez/metadata/fer/AFEW-experiments/i3d/2019-06-07/16/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/AFEW/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/AFEW/ri_caug
scp -r  dl-03:/work/mrodriguez/metadata/fer/AFEW-experiments/i3d/2019-06-07/18/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/AFEW/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/CK/ri_syn
scp -r  dl-03:/work/mrodriguez/metadata/fer/CK-experiments/resnet3d_18/2019-06-16/5/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/CK/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/CK/ft
scp -r  dl-03:/work/mrodriguez/metadata/fer/CK-experiments/resnet3d_18/2019-06-06/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/CK/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/CK/ft_syn
scp -r  dl-03:/work/mrodriguez/metadata/fer/CK-experiments/resnet3d_18/2019-06-16/6/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/CK/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MMI/ri_caug
scp -r  dl-03:/work/mrodriguez/metadata/fer/MMI-experiments/resnet3d_18/2019-06-06/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MMI/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MMI/ft
scp -r  dl-03:/work/mrodriguez/metadata/fer/MMI-experiments/resnet3d_18/2019-06-06/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MMI/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MMI/ft_caug
scp -r  dl-03:/work/mrodriguez/metadata/fer/MMI-experiments/resnet3d_18/2019-06-06/6/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MMI/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/CK/ri_caug
scp -r  dl-03:/work/mrodriguez/metadata/fer/CK-experiments/resnet3d_101/2019-05-05/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/CK/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MMI/ri_caug
scp -r  dl-03:/work/mrodriguez/metadata/fer/MMI-experiments/resnet3d_101/2019-05-05/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MMI/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MMI/ft_caug
scp -r  dl-03:/work/mrodriguez/metadata/fer/MMI-experiments/resnet3d_101/2019-05-03/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MMI/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/AFEW/ri_caug
scp -r  dl-03:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet3d_101/2019-06-08/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/AFEW/ri_caug
