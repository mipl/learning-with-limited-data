mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/MUG/ri
scp -r  dl-09:/work/mrodriguez/metadata/fer/MUG-experiments/vgg16_lstm/2019-05-30/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/MUG/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/MUG/ri_caug
scp -r  dl-09:/work/mrodriguez/metadata/fer/MUG-experiments/vgg16_lstm/2019-06-04/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/MUG/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MUG/ri
scp -r  dl-09:/work/mrodriguez/metadata/fer/MUG-experiments/inceptionv3_lstm/2019-06-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MUG/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/OULU/ft_caug
scp -r  dl-09:/work/mrodriguez/metadata/fer/OULU-experiments/resnet18_lstm/2019-05-18/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/OULU/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/CK/ri_caug
scp -r  dl-09:/work/mrodriguez/metadata/fer/CK-experiments/resnet101_lstm/2019-05-23/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/CK/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MMI/ft_syn
scp -r  dl-09:/work/mrodriguez/metadata/fer/MMI-experiments/c3d/2019-07-10/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MMI/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MMI/ri_caug
scp -r  dl-09:/work/mrodriguez/metadata/fer/MMI-experiments/i3d/2019-05-08/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MMI/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MUG/ri_syn
scp -r  dl-09:/work/mrodriguez/metadata/fer/MUG-experiments/resnet3d_18/2019-07-07/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MUG/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MUG/ft_syn
scp -r  dl-09:/work/mrodriguez/metadata/fer/MUG-experiments/resnet3d_18/2019-07-05/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MUG/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/CK/ri_syn
scp -r  dl-09:/work/mrodriguez/metadata/fer/CK-experiments/resnet3d_101/2019-06-16/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/CK/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/OULU/ri_caug
scp -r  dl-09:/work/mrodriguez/metadata/fer/OULU-experiments/resnet3d_101/2019-05-29/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/OULU/ri_caug
