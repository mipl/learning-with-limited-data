mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/CK/ri_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/vgg16_lstm/2019-06-19/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/CK/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/CK/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/vgg16_lstm/2019-06-19/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/CK/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/MMI/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/MMI-experiments/vgg16_lstm/2019-07-14/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/MMI/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/OULU/ri
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/vgg16_lstm/2019-02-18/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/OULU/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/OULU/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/vgg16_lstm/2019-07-17/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/OULU/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/MUG/ri_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/vgg16_lstm/2019-07-10/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/MUG/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/MUG/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/vgg16_lstm/2019-06-06/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/MUG/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/vgg16_lstm/2019-07-27/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/MUG/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/AFEW/ri_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/vgg16_lstm/2019-06-07/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/AFEW/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/AFEW/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/vgg16_lstm/2019-08-27/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/AFEW/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/AFEW/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/vgg16_lstm/2019-06-07/6/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/AFEW/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/CK/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/inceptionv3_lstm/2019-06-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/CK/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MMI/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/MMI-experiments/inceptionv3_lstm/2019-06-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MMI/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/OULU/ri_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/inceptionv3_lstm/2019-02-20/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/OULU/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/OULU/ri_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/inceptionv3_lstm/2019-07-18/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/OULU/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/OULU/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/inceptionv3_lstm/2019-02-20/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/OULU/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/OULU/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/inceptionv3_lstm/2019-07-18/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/OULU/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MUG/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/inceptionv3_lstm/2019-06-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MUG/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/inceptionv3_lstm/2019-07-10/3/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MUG/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/inceptionv3_lstm/2019-07-10/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MUG/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/AFEW/ri
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/inceptionv3_lstm/2019-08-28/3/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/AFEW/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/AFEW/ri_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/inceptionv3_lstm/2019-08-28/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/AFEW/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/AFEW/ri_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/inceptionv3_lstm/2020-01-02/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/AFEW/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/AFEW/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/inceptionv3_lstm/2019-08-26/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/AFEW/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/AFEW/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/inceptionv3_lstm/2019-08-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/AFEW/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/AFEW/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/inceptionv3_lstm/2020-01-02/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/AFEW/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/CK/ri
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/resnet18_lstm/2019-05-14/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/CK/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/CK/ri_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/resnet18_lstm/2019-05-15/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/CK/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/CK/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/resnet18_lstm/2019-05-14/6/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/CK/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/CK/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/resnet18_lstm/2019-05-15/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/CK/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/CK/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/resnet18_lstm/2019-06-16/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/CK/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/OULU/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/resnet18_lstm/2019-07-17/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/OULU/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MUG/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/resnet18_lstm/2019-05-18/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MUG/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/resnet18_lstm/2019-05-18/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/AFEW/ri_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet18_lstm/2020-01-12/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/AFEW/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/AFEW/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet18_lstm/2019-06-07/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/AFEW/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/AFEW/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet18_lstm/2019-06-07/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/AFEW/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/CK/ri_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/resnet101_lstm/2019-06-19/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/CK/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/CK/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/resnet101_lstm/2019-05-10/20/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/CK/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/CK/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/resnet101_lstm/2019-05-15/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/CK/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/CK/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/resnet101_lstm/2019-06-19/3/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/CK/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MMI/ri
scp -r  dl-08:/work/mrodriguez/metadata/fer/MMI-experiments/resnet101_lstm/2019-05-10/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MMI/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MMI/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/MMI-experiments/resnet101_lstm/2019-05-13/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MMI/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MMI/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/MMI-experiments/resnet101_lstm/2019-05-15/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MMI/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MMI/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/MMI-experiments/resnet101_lstm/2019-07-23/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MMI/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/OULU/ri
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/resnet101_lstm/2019-05-22/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/OULU/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/OULU/ri_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/resnet101_lstm/2019-05-26/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/OULU/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/OULU/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/resnet101_lstm/2019-05-22/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/OULU/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/OULU/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/resnet101_lstm/2019-07-19/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/OULU/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MUG/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/./metadata/MUG-experiments/resnet101_lstm/2019-05-30/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MUG/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/resnet101_lstm/2019-05-28/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MUG/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/resnet101_lstm/2019-07-29/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MUG/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/AFEW/ri_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet101_lstm/2019-06-08/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/AFEW/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/AFEW/ri_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet101_lstm/2020-01-13/3/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/AFEW/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/AFEW/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet101_lstm/2019-08-26/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/AFEW/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/AFEW/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet101_lstm/2020-01-13/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/AFEW/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/OULU/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/vgg16_lstm/2019-02-16/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/OULU/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MUG/ri_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/c3d/2019-06-02/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MUG/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MUG/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/c3d/2019-05-30/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/AFEW/ri
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/c3d/2019-06-06/8/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/AFEW/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/AFEW/ri_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/c3d/2019-12-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/AFEW/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/AFEW/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/c3d/2019-12-31/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/AFEW/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/OULU/ri
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/c3d_block_lstm/2020-01-21/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/OULU/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/OULU/ri_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/c3d_block_lstm/2020-01-21/3/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/OULU/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/OULU/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/c3d_block_lstm/2019-02-18/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/OULU/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MUG/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/c3d_block_lstm/2019-06-03/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MUG/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/c3d_block_lstm/2019-07-05/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MUG/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/AFEW/ri_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/c3d_block_lstm/2019-08-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/AFEW/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/AFEW/ri_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/c3d_block_lstm/2019-12-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/AFEW/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/AFEW/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/c3d_block_lstm/2019-08-27/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/AFEW/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/CK/ri
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/i3d/2019-05-08/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/CK/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/CK/ri_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/i3d/2019-05-08/6/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/CK/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/CK/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/i3d/2019-05-08/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/CK/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/CK/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/i3d/2019-05-08/8/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/CK/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/CK/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/i3d/2019-06-19/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/CK/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MMI/ri
scp -r  dl-08:/work/mrodriguez/metadata/fer/MMI-experiments/i3d/2019-05-05/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MMI/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MMI/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/MMI-experiments/i3d/2019-05-05/6/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MMI/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MMI/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/MMI-experiments/i3d/2019-05-11/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MMI/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MMI/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/MMI-experiments/i3d/2019-08-15/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MMI/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/OULU/ri_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/i3d/2019-08-16/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/OULU/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/OULU/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/i3d/2019-05-08/6/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/OULU/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/OULU/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/i3d/2019-05-08/8/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/OULU/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/OULU/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/i3d/2019-08-15/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/OULU/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MUG/ri
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/i3d/2019-05-11/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MUG/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MUG/ri_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/i3d/2019-05-12/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MUG/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MUG/ri_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/i3d/2019-08-16/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MUG/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MUG/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/i3d/2019-05-18/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MUG/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/i3d/2019-05-22/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MUG/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/i3d/2019-08-15/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MUG/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/AFEW/ri_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/i3d/2020-01-01/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/AFEW/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/AFEW/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/i3d/2020-01-01/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/AFEW/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/CK/ri
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/resnet3d_18/2019-06-06/10/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/CK/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/CK/ri_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/resnet3d_18/2019-06-06/24/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/CK/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/OULU/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/resnet3d_18/2019-06-06/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/OULU/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MUG/ri
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/resnet3d_18/2019-06-06/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MUG/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MUG/ri_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/resnet3d_18/2019-06-06/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MUG/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/AFEW/ri_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet3d_18/2020-01-13/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/AFEW/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/AFEW/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet3d_18/2020-01-13/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/AFEW/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/CK/ri
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/resnet3d_101/2019-05-05/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/CK/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/CK/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/resnet3d_101/2019-05-03/10/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/CK/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/CK/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/CK-experiments/resnet3d_101/2019-05-03/14/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/CK/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MMI/ri
scp -r  dl-08:/work/mrodriguez/metadata/fer/MMI-experiments/resnet3d_101/2019-05-05/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MMI/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MMI/ri_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/MMI-experiments/resnet3d_101/2019-07-14/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MMI/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MMI/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/MMI-experiments/resnet3d_101/2019-05-03/10/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MMI/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MMI/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/MMI-experiments/resnet3d_101/2019-07-14/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MMI/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/OULU/ri
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/resnet3d_101/2019-05-05/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/OULU/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/OULU/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/resnet3d_101/2019-05-05/6/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/OULU/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/OULU/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/OULU-experiments/resnet3d_101/2019-05-05/10/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/OULU/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MUG/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/MUG-experiments/resnet3d_101/2019-07-05/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MUG/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/AFEW/ri_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet3d_101/2020-01-02/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/AFEW/ri_syn
