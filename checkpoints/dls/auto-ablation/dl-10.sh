mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/OULU/ft_syn
scp -r  dl-10:/work/mrodriguez/metadata/fer/OULU-experiments/vgg16_lstm/2019-07-17/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/OULU/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/AFEW/ri_caug
scp -r  dl-10:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet18_lstm/2019-06-07/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/AFEW/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MUG/ri_syn
scp -r  dl-10:/work/mrodriguez/metadata/fer/MUG-experiments/resnet101_lstm/2019-08-16/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/MUG/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/OULU/ft_syn
scp -r  dl-10:/work/mrodriguez/metadata/fer/OULU-experiments/c3d/2020-01-21/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/OULU/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MUG/ri
scp -r  dl-10:/work/mrodriguez/metadata/fer/MUG-experiments/c3d/2019-05-26/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MUG/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/AFEW/ft_caug
scp -r  dl-10:/work/mrodriguez/metadata/fer/AFEW-experiments/c3d/2019-06-06/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/AFEW/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/CK/ri_syn
scp -r  dl-10:/work/mrodriguez/metadata/fer/CK-experiments/c3d_block_lstm/2019-06-19/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/CK/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/CK/ft_syn
scp -r  dl-10:/work/mrodriguez/metadata/fer/CK-experiments/c3d_block_lstm/2019-06-19/3/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/CK/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MUG/ri_caug
scp -r  dl-10:/work/mrodriguez/metadata/fer/MUG-experiments/c3d_block_lstm/2019-06-02/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MUG/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MMI/ri_syn
scp -r  dl-10:/work/mrodriguez/metadata/fer/MMI-experiments/i3d/2019-08-15/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/MMI/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/OULU/ft
scp -r  dl-10:/work/mrodriguez/metadata/fer/OULU-experiments/resnet3d_18/2019-06-06/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/OULU/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MUG/ri_syn
scp -r  dl-10:/work/mrodriguez/metadata/fer/MUG-experiments/resnet3d_101/2019-07-14/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MUG/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MUG/ft
scp -r  dl-10:/work/mrodriguez/metadata/fer/MUG-experiments/resnet3d_101/2019-05-22/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/AFEW/ri
scp -r  dl-10:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet3d_101/2019-06-07/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/AFEW/ri
