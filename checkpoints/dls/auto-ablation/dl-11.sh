mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/AFEW/ft_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/AFEW-experiments/vgg16_lstm/2019-12-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/VGG16-lstm/AFEW/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/CK/ri_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/CK-experiments/inceptionv3_lstm/2019-06-19/10/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/CK/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MMI/ft
scp -r  dl-11:/work/mrodriguez/metadata/fer/MMI-experiments/inceptionv3_lstm/2019-02-20/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MMI/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/OULU/ft
scp -r  dl-11:/work/mrodriguez/metadata/fer/OULU-experiments/inceptionv3_lstm/2019-02-20/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/OULU/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MUG/ri_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/MUG-experiments/inceptionv3_lstm/2019-07-07/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MUG/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/OULU/ri_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/OULU-experiments/resnet18_lstm/2019-07-17/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/OULU/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MUG/ri_caug
scp -r  dl-11:/work/mrodriguez/metadata/fer/MUG-experiments/resnet18_lstm/2019-05-18/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MUG/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MUG/ri_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/MUG-experiments/resnet18_lstm/2019-07-05/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MUG/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MUG/ft_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/MUG-experiments/resnet18_lstm/2019-07-05/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet18-lstm/MUG/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/CK/ft_caug
scp -r  dl-11:/work/mrodriguez/metadata/fer/CK-experiments/c3d/2019-02-17/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/CK/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MUG/ft_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/MUG-experiments/c3d/2019-07-02/11/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/C3D/MUG/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MMI/ri_caug
scp -r  dl-11:/work/mrodriguez/metadata/fer/MMI-experiments/c3d_block_lstm/2019-02-16/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MMI/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MUG/ft
scp -r  dl-11:/work/mrodriguez/metadata/fer/MUG-experiments/c3d_block_lstm/2019-06-02/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/CK/ri_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/CK-experiments/i3d/2019-06-16/3/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/I3D/CK/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MUG/ft_caug
scp -r  dl-11:/work/mrodriguez/metadata/fer/MUG-experiments/resnet3d_18/2019-06-06/6/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/AFEW/ft
scp -r  dl-11:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet3d_18/2019-06-07/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-18/AFEW/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/CK/ft_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/CK-experiments/resnet3d_101/2019-06-16/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/CK/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MUG/ft_caug
scp -r  dl-11:/work/mrodriguez/metadata/fer/MUG-experiments/resnet3d_101/2019-05-22/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/MUG/ft_caug
