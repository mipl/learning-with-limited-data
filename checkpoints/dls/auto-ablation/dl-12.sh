mkdir -p /work/mrodriguez/limited-data-checkpoints/Inceptionv3-lstm/CK/ft_syn
scp -r  dl-12:/work/mrodriguez/metadata/fer/CK-experiments/inceptionv3_lstm/2019-06-19/3/checkpoints /work/mrodriguez/limited-data-checkpoints/Inceptionv3-lstm/CK/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/ResNet101-lstm/AFEW/ft_caug
scp -r  dl-12:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet101_lstm/2019-06-08/2/checkpoints /work/mrodriguez/limited-data-checkpoints/ResNet101-lstm/AFEW/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/ResNet3D-18/AFEW/ri
scp -r  dl-12:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet3d_18/2019-06-07/2/checkpoints /work/mrodriguez/limited-data-checkpoints/ResNet3D-18/AFEW/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/ResNet3D-18/AFEW/ri_caug
scp -r  dl-12:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet3d_18/2019-06-07/4/checkpoints /work/mrodriguez/limited-data-checkpoints/ResNet3D-18/AFEW/ri_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/ResNet3D-18/AFEW/ft_caug
scp -r  dl-12:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet3d_18/2019-06-07/6/checkpoints /work/mrodriguez/limited-data-checkpoints/ResNet3D-18/AFEW/ft_caug
