mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/CK/ri
scp -r  dl-17:/work/mrodriguez/metadata/fer/CK-experiments/inceptionv3_lstm/2019-08-30/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/CK/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MMI/ri_syn
scp -r  dl-17:/work/mrodriguez/metadata/fer/MMI-experiments/inceptionv3_lstm/2019-07-17/6/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MMI/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MMI/ft_syn
scp -r  dl-17:/work/mrodriguez/metadata/fer/MMI-experiments/inceptionv3_lstm/2019-07-17/7/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/Inceptionv3-lstm/MMI/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/OULU/ri_syn
scp -r  dl-17:/work/mrodriguez/metadata/fer/OULU-experiments/resnet101_lstm/2019-07-19/4/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/OULU/ri_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/AFEW/ri
scp -r  dl-17:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet101_lstm/2019-08-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet101-lstm/AFEW/ri
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/OULU/ft_syn
scp -r  dl-17:/work/mrodriguez/metadata/fer/OULU-experiments/c3d_block_lstm/2019-07-17/6/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/OULU/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/AFEW/ft_syn
scp -r  dl-17:/work/mrodriguez/metadata/fer/AFEW-experiments/c3d_block_lstm/2019-12-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/c3d-block-lstm/AFEW/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/AFEW/ft_syn
scp -r  dl-17:/work/mrodriguez/metadata/fer/AFEW-experiments/resnet3d_101/2020-01-02/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-ablation/ResNet3D-101/AFEW/ft_syn
