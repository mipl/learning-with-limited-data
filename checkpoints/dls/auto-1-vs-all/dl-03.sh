mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/MMI/ft_caug
scp -r  dl-03:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/vgg16_lstm/2019-07-29/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/MMI/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/OULU/ft_caug
scp -r  dl-03:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/vgg16_lstm/2019-07-30/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/OULU/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/MMI/ft_caug_syn
scp -r  dl-03:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/vgg16_lstm/2019-08-12/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/MMI/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/MMI/ft
scp -r  dl-03:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/resnet101_lstm/2019-07-25/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/MMI/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/OULU/ft_syn
scp -r  dl-03:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/resnet101_lstm/2019-07-26/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/OULU/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/MUG/ft
scp -r  dl-03:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/c3d/2019-07-21/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/MMI/ft_caug
scp -r  dl-03:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/c3d/2019-07-30/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/MMI/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/CK/ft_syn
scp -r  dl-03:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/c3d/2019-07-19/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/CK/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/MUG/ft_syn
scp -r  dl-03:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/c3d/2019-07-19/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/MUG/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/CK/ft_caug
scp -r  dl-03:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/c3d_block_lstm/2019-07-29/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/CK/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/OULU/ft_syn
scp -r  dl-03:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/c3d_block_lstm/2019-07-21/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/OULU/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/CK/ft
scp -r  dl-03:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/resnet3d_101/2019-07-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/CK/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/OULU/ft
scp -r  dl-03:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/resnet3d_101/2019-07-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/OULU/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/AFEW/ft
scp -r  dl-03:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/resnet3d_101/2019-07-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/AFEW/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/AFEW/ft_syn
scp -r  dl-03:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/resnet3d_101/2019-07-28/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/AFEW/ft_syn
