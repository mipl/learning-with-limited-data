mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/CK/ft_syn
scp -r  dl-09:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/vgg16_lstm/2019-07-26/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/CK/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/OULU/ft
scp -r  dl-09:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/resnet18_lstm/2019-07-21/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/OULU/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/OULU/ft_caug
scp -r  dl-09:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/resnet18_lstm/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/OULU/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/CK/ft_caug_syn
scp -r  dl-09:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/resnet18_lstm/2019-08-05/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/CK/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/MMI/ft_caug_syn
scp -r  dl-09:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/resnet18_lstm/2019-08-05/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/MMI/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/CK/ft_caug
scp -r  dl-09:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/resnet101_lstm/2019-07-30/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/CK/ft_caug
