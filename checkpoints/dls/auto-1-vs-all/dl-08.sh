mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/AFEW/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/vgg16_lstm/2019-07-27/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/AFEW/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/OULU/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/vgg16_lstm/2019-08-02/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/OULU/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/MUG/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/inceptionv3_lstm/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/CK/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/resnet18_lstm/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/CK/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/MUG/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/resnet18_lstm/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/MUG/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/resnet18_lstm/2019-07-24/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/MUG/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/AFEW/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/resnet18_lstm/2019-07-24/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/AFEW/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/CK/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/resnet101_lstm/2019-07-25/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/CK/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/MUG/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/resnet101_lstm/2019-08-01/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/CK/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/resnet101_lstm/2019-07-25/3/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/CK/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/MMI/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/resnet101_lstm/2019-07-25/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/MMI/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/MUG/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/resnet101_lstm/2019-07-26/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/MUG/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/CK/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/resnet101_lstm/2019-08-05/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/CK/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/MMI/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/resnet101_lstm/2019-08-05/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/MMI/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/OULU/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/resnet101_lstm/2019-08-05/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/OULU/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/MUG/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/resnet101_lstm/2019-08-05/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/MUG/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/AFEW/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/resnet101_lstm/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/AFEW/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/CK/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/c3d/2019-07-30/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/CK/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/CK/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/c3d/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/CK/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/MMI/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/c3d/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/MMI/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/OULU/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/c3d/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/OULU/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/MUG/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/c3d/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/MUG/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/CK/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/c3d_block_lstm/2019-07-18/31/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/CK/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/OULU/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/c3d_block_lstm/2019-07-30/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/OULU/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/MMI/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/c3d_block_lstm/2019-07-21/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/MMI/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/MUG/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/c3d_block_lstm/2019-07-21/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/MUG/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/MMI/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/c3d_block_lstm/2019-08-08/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/MMI/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/OULU/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/c3d_block_lstm/2019-08-08/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/OULU/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/MUG/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/c3d_block_lstm/2019-08-08/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/MUG/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/AFEW/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/c3d_block_lstm/2019-08-08/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/AFEW/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/OULU/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/i3d/2019-07-29/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/OULU/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/MUG/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/i3d/2019-07-30/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/CK/ft_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/i3d/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/CK/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/CK/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/i3d/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/CK/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/MMI/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/i3d/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/MMI/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/OULU/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/i3d/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/OULU/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/MUG/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/i3d/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/MUG/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/MUG/ft
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/resnet3d_18/2019-07-19/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/CK/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/resnet3d_18/2019-07-30/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/CK/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/MUG/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/resnet3d_18/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/MMI/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/resnet3d_101/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/MMI/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/MUG/ft_caug
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/resnet3d_101/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/AFEW/ft_caug_syn
scp -r  dl-08:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/resnet3d_101/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/AFEW/ft_caug_syn
