mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/MUG/ft_caug
scp -r  dl-17:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/vgg16_lstm/2019-07-29/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/MMI/ft_caug
scp -r  dl-17:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/inceptionv3_lstm/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/MMI/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/OULU/ft_caug
scp -r  dl-17:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/inceptionv3_lstm/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/OULU/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/MMI/ft_caug
scp -r  dl-17:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/resnet101_lstm/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/MMI/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/OULU/ft
scp -r  dl-17:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/c3d/2019-07-19/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/OULU/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/CK/ft
scp -r  dl-17:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/resnet3d_18/2019-07-19/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/CK/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/AFEW/ft_caug
scp -r  dl-17:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/resnet3d_101/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/AFEW/ft_caug
