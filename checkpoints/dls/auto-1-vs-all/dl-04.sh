mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/MMI/ft_syn
scp -r  dl-04:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/c3d/2019-07-19/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/MMI/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/MUG/ft_caug_syn
scp -r  dl-04:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/resnet3d_18/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/MUG/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/AFEW/ft_caug_syn
scp -r  dl-04:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/resnet3d_18/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/AFEW/ft_caug_syn
