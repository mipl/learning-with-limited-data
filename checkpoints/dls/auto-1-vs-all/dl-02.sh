mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/CK/ft
scp -r  dl-02:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/c3d/2019-07-18/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/CK/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/OULU/ft_caug
scp -r  dl-02:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/c3d/2019-07-30/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/OULU/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/OULU/ft_syn
scp -r  dl-02:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/c3d/2019-07-19/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/OULU/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/AFEW/ft_syn
scp -r  dl-02:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/c3d_block_lstm/2019-07-18/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/AFEW/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/OULU/ft_caug
scp -r  dl-02:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/resnet3d_18/2019-07-30/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/OULU/ft_caug
