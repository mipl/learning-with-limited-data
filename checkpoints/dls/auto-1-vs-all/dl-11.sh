mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/MUG/ft
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/vgg16_lstm/2019-07-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/AFEW/ft_caug
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/vgg16_lstm/2019-07-29/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/AFEW/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/CK/ft_caug_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/vgg16_lstm/2019-08-02/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/CK/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/OULU/ft_caug_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/resnet18_lstm/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/OULU/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/MUG/ft_caug_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/resnet18_lstm/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/MUG/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/OULU/ft
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/resnet101_lstm/2019-07-25/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/OULU/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/MMI/ft
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/c3d/2019-07-18/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/MMI/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/AFEW/ft_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/c3d/2019-07-19/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/AFEW/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/AFEW/ft_caug_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/c3d/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/AFEW/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/MUG/ft
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/c3d_block_lstm/2019-07-18/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/MMI/ft
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/resnet3d_101/2019-07-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/MMI/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/CK/ft_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/resnet3d_101/2019-07-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/CK/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/MMI/ft_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/resnet3d_101/2019-07-28/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/MMI/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/OULU/ft_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/resnet3d_101/2019-07-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/OULU/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/CK/ft_caug_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/resnet3d_101/2019-08-05/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/CK/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/MMI/ft_caug_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/resnet3d_101/2019-08-05/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/MMI/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/OULU/ft_caug_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/resnet3d_101/2019-08-05/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/OULU/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/MUG/ft_caug_syn
scp -r  dl-11:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/resnet3d_101/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/MUG/ft_caug_syn
