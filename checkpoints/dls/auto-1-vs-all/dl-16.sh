mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/MMI/ft_syn
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/vgg16_lstm/2019-07-27/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/MMI/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/MUG/ft_caug_syn
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/vgg16_lstm/2019-08-02/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/MUG/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/AFEW/ft_caug_syn
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/vgg16_lstm/2019-08-02/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/AFEW/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/MMI/ft
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/inceptionv3_lstm/2019-07-27/3/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/MMI/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/OULU/ft
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/inceptionv3_lstm/2019-07-27/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/OULU/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/MUG/ft
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/inceptionv3_lstm/2019-07-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/AFEW/ft
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/inceptionv3_lstm/2019-07-28/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/AFEW/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/CK/ft_caug
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/inceptionv3_lstm/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/CK/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/MMI/ft_syn
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/inceptionv3_lstm/2019-07-27/5/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/MMI/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/MMI/ft_caug
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/resnet18_lstm/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/MMI/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/OULU/ft_caug
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/resnet101_lstm/2019-07-31/3/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/OULU/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/CK/ft
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/i3d/2019-07-29/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/CK/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/MMI/ft
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/i3d/2019-07-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/MMI/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/CK/ft_caug
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/i3d/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/CK/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/MMI/ft_caug
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/i3d/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/MMI/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/OULU/ft_caug
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/i3d/2019-08-01/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/OULU/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/MUG/ft_caug
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/i3d/2019-08-01/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/MMI/ft_syn
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/i3d/2019-07-28/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/MMI/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/OULU/ft_syn
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/i3d/2019-07-29/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/OULU/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/MUG/ft_syn
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/i3d/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/I3D/MUG/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/MMI/ft_caug
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/resnet3d_18/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/MMI/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/AFEW/ft_caug
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/resnet3d_18/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/AFEW/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/MUG/ft
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/resnet3d_101/2019-07-28/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/CK/ft_caug
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/resnet3d_101/2019-07-31/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/CK/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/OULU/ft_caug
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/resnet3d_101/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/OULU/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/MUG/ft_syn
scp -r  dl-16:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/resnet3d_101/2019-07-28/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-101/MUG/ft_syn
