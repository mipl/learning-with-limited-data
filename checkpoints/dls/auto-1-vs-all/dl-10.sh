mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/CK/ft_caug
scp -r  dl-10:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/vgg16_lstm/2019-07-29/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/VGG16-lstm/CK/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/CK/ft_caug_syn
scp -r  dl-10:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/inceptionv3_lstm/2019-08-02/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/CK/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/MMI/ft_caug_syn
scp -r  dl-10:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/inceptionv3_lstm/2019-08-03/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/MMI/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/MUG/ft_caug_syn
scp -r  dl-10:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/inceptionv3_lstm/2019-08-04/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/MUG/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/AFEW/ft_caug_syn
scp -r  dl-10:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/inceptionv3_lstm/2019-08-05/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/AFEW/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/AFEW/ft
scp -r  dl-10:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/resnet18_lstm/2019-07-21/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/AFEW/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/AFEW/ft_caug
scp -r  dl-10:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/resnet18_lstm/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/AFEW/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/AFEW/ft_caug_syn
scp -r  dl-10:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/resnet18_lstm/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet18-lstm/AFEW/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/MUG/ft_caug
scp -r  dl-10:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/c3d_block_lstm/2019-07-30/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/CK/ft_caug_syn
scp -r  dl-10:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/c3d_block_lstm/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/CK/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/CK/ft_caug_syn
scp -r  dl-10:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/resnet3d_18/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/CK/ft_caug_syn
