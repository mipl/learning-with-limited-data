mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/AFEW/ft
scp -r  dl-06:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/resnet101_lstm/2019-07-25/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/AFEW/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/AFEW/ft_syn
scp -r  dl-06:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/resnet101_lstm/2019-07-25/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/AFEW/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/OULU/ft_caug_syn
scp -r  dl-06:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/resnet3d_18/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/OULU/ft_caug_syn
