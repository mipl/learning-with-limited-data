mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/CK/ft
scp -r  dl-14:/work/mrodriguez/metadata/fer/cross_1vall_train_CK-experiments/inceptionv3_lstm/2019-07-27/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/CK/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/AFEW/ft_caug
scp -r  dl-14:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/inceptionv3_lstm/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/AFEW/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/OULU/ft_syn
scp -r  dl-14:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/inceptionv3_lstm/2019-07-28/2/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/OULU/ft_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/OULU/ft_caug_syn
scp -r  dl-14:/work/mrodriguez/metadata/fer/cross_1vall_train_OULU-experiments/inceptionv3_lstm/2019-08-03/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/Inceptionv3-lstm/OULU/ft_caug_syn
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/MUG/ft
scp -r  dl-14:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/resnet101_lstm/2019-07-25/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/MUG/ft
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/AFEW/ft_caug
scp -r  dl-14:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/resnet101_lstm/2019-08-01/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet101-lstm/AFEW/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/MUG/ft_caug
scp -r  dl-14:/work/mrodriguez/metadata/fer/cross_1vall_train_MUG-experiments/c3d/2019-07-30/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/MUG/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/AFEW/ft_caug
scp -r  dl-14:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/c3d/2019-07-30/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/C3D/AFEW/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/MMI/ft_caug
scp -r  dl-14:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/c3d_block_lstm/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/MMI/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/AFEW/ft_caug
scp -r  dl-14:/work/mrodriguez/metadata/fer/cross_1vall_train_AFEW-experiments/c3d_block_lstm/2019-07-31/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/c3d-block-lstm/AFEW/ft_caug
mkdir -p /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/MMI/ft_caug_syn
scp -r  dl-14:/work/mrodriguez/metadata/fer/cross_1vall_train_MMI-experiments/resnet3d_18/2019-08-06/1/checkpoints /work/mrodriguez/limited-data-checkpoints/auto-1-vs-all/ResNet3D-18/MMI/ft_caug_syn
