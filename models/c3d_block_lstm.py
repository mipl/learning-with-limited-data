import torch

from models.base_model import BaseModel
from models.c3d import C3D

class Flatten(torch.nn.Module):
    def forward(self, x):
        x = x.view(x.size()[0], -1)
        return x

class ClassifierModule(torch.nn.Module):
    def __init__(self, in_size, n_labels, dropout_prob):
        super(ClassifierModule, self).__init__()
        # self.ReLU = torch.nn.ReLU()
        self.fc1 = torch.nn.Linear(1024, 512)
        self.fc2 = torch.nn.Linear(512,256)
        self.fc_out = torch.nn.Linear(256, n_labels)

    def forward(self, x):
        model = self.fc1(x)
        model = self.fc2(model)
        model = self.fc_out(model)

        return model

class C3dBlockLstmNet(BaseModel):
    def __init__(self, n_labels, dropout_prob, pretrained, *args, **kwargs):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        super(C3dBlockLstmNet, self).__init__(*args, **kwargs)
        self.c3d_block = C3D(dropout_prob=dropout_prob, pretrained=pretrained, classifier=False, tpad=False)

        self.flatten = Flatten()

        self.lstm = torch.nn.LSTM(input_size=8192,
                                  hidden_size=1024,
                                  num_layers=1,
                                  dropout=0)


        self.classifier = ClassifierModule(in_size=2048, n_labels=n_labels, dropout_prob=dropout_prob)

        self.criterion = torch.nn.CrossEntropyLoss()

    def forward(self, x):
        blocks = torch.split(x, 1, dim=1)

        fvectors = []
        for block in blocks:
            block = torch.squeeze(block, dim=1)
            fvectors.append(self.flatten(self.c3d_block(block)))

        seq = torch.stack(fvectors)

        # output, (hn, cn) = self.lstm(seq, (h0, c0))
        output, (hn, cn) = self.lstm(seq)

        y_pred = self.classifier(output[-1])


        return y_pred

    def loss(self, y_preds, y):
        loss = self.criterion(y_preds, y)

        return loss
