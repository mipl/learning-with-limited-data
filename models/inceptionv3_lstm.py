import torch
import torchvision

from models.base_model import BaseModel
from models.modules.classifier_module import ClassifierModule_1l

class InceptionV3Module(torch.nn.Module):
    def __init__(self, pretrained=True):
        super(InceptionV3Module, self).__init__()
        self.inception_v3 = torchvision.models.inception_v3(pretrained=pretrained)
        # self.inception_v3.fc = torch.nn.Linear(self.inception_v3.fc.in_features, out_features)

    def forward(self, x):
        return self.inception_v3(x)


class InceptionV3LstmNet(BaseModel):
    def __init__(self, n_labels, dropout_prob=0.0, pretrained=True, *args, **kwargs):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        super(InceptionV3LstmNet, self).__init__(*args, **kwargs)
        lstm_out_features = 512
        fd_out_features=1000


        self.feature_descriptor = InceptionV3Module(pretrained=pretrained)

        self.lstm = torch.nn.LSTM(input_size=fd_out_features,
                                  hidden_size=lstm_out_features,
                                  num_layers=1,
                                  dropout=dropout_prob)

        self.aux_lstm = torch.nn.LSTM(input_size=fd_out_features,
                                      hidden_size=lstm_out_features,
                                      num_layers=1,
                                      dropout=dropout_prob)

        self.classifier = ClassifierModule_1l(in_size=lstm_out_features, n_labels=n_labels)
        self.aux_classifier = ClassifierModule_1l(in_size=lstm_out_features, n_labels=n_labels)

        self.criterion = torch.nn.CrossEntropyLoss()



    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        frames = torch.split(x, 1, dim=2)

        fvectors = []
        if self.training:
            aux_fvectors = []

        for img in frames:
            img = torch.squeeze(img, dim=2)
            if self.training:
                fvector, aux_fvector = self.feature_descriptor(img)
                aux_fvectors.append(aux_fvector)
            else:
                fvector = self.feature_descriptor(img)
            fvectors.append(fvector)

        seq = torch.stack(fvectors)
        output, (hn, cn) = self.lstm(seq)
        y_pred = self.classifier(output[-1])

        if self.training:
            aux_seq = torch.stack(aux_fvectors)
            aux_output, (aux_hn, aux_cn) = self.aux_lstm(aux_seq)
            aux_y_pred = self.aux_classifier(aux_output[-1])

            return y_pred, aux_y_pred

        # print(seq.shape)
        # output, (hn, cn) = self.lstm(seq, (h0, c0))
        return y_pred


    def loss(self, y_preds, y):
        if self.training:
            y_pred, aux_y_pred = y_preds

            loss_1 = self.criterion(y_pred, y)
            loss_2 = self.criterion(aux_y_pred, y)

            loss = loss_1 + 0.4 * loss_2
        else:
            loss = self.criterion(y_preds, y)

        return loss
