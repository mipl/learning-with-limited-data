import torch
import torchvision

from models.base_model import BaseModel


class ResNet101Module(torch.nn.Module):
    def __init__(self, out_features=2048, pretrained=True):
        super(ResNet101Module, self).__init__()
        self.resnet101 = torchvision.models.resnet101(pretrained=pretrained)
        self.resnet101.fc = torch.nn.Linear(2048, out_features)

    def forward(self, x):
        return self.resnet101(x)

class ClassifierModule(torch.nn.Module):
    def __init__(self, in_features, n_labels, dropout_prob):
        super(ClassifierModule, self).__init__()
        self.linear1 = torch.nn.Linear(in_features, 512)
        self.linear2 = torch.nn.Linear(512,256)
        self.linear_out = torch.nn.Linear(256, n_labels)

    def forward(self, x):
        y_pred = self.linear1(x)
        y_pred = self.linear2(y_pred)
        y_pred = self.linear_out(y_pred)

        return y_pred


class ResNet101LstmNet(BaseModel):
    def __init__(self, n_labels, dropout_prob=0.0, pretrained=True, *args, **kwargs):
        """
        In the constructor we instantiate two nn.Linear modules and assign them as
        member variables.
        """
        super(ResNet101LstmNet, self).__init__(*args, **kwargs)
        fd_out_features = 512
        lstm_out_features = 512


        self.feature_descriptor = ResNet101Module(out_features=fd_out_features, pretrained=pretrained)



        self.lstm = torch.nn.LSTM(input_size=fd_out_features,
                                  hidden_size=lstm_out_features,
                                  num_layers=1,
                                  dropout=dropout_prob)

        self.classifier = ClassifierModule(in_features=lstm_out_features, n_labels=n_labels, dropout_prob=dropout_prob)

        self.criterion = torch.nn.CrossEntropyLoss()



    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        frames = torch.split(x, 1, dim=2)

        fvectors = []
        for img in frames:
            img = torch.squeeze(img, dim=2)
            fvector = self.feature_descriptor(img)
            fvectors.append(fvector)

        seq = torch.stack(fvectors)
        # output, (hn, cn) = self.lstm(seq, (h0, c0))
        output, (hn, cn) = self.lstm(seq)

        y_pred = self.classifier(output[-1])

        return y_pred

    def loss(self, y_preds, y):
        loss = self.criterion(y_preds, y)

        return loss
