import torch

def get_dataset_by_model(model_name, dataset_name, *args, **kwargs):
    if model_name in ["c3d", "inceptionv3_lstm", "vgg16_lstm", "resnet3d_101","resnet3d_18", "i3d", "resnet101_lstm", "resnet18_lstm"]:
        return 'video'
    elif model_name in ["c3d_block_lstm"]:
        return "blocks"


def get_model_params(model_name, dataset_name, n_labels, pretrained, bh=False, dropout_prob=0.5):
    n_gpus = torch.cuda.device_count() if torch.cuda.device_count() > 0 else 1

    batch_size = 10
    img_size = 100
    n_frames = 25
    n_blocks = 5
    frames_per_block = 5
    model_kwargs = {}

    if model_name == "inceptionv3_lstm":
        batch_size = 3
        img_size = 300
        model_kwargs = {}

    elif  model_name == "vgg16_lstm":
        batch_size = 3
        img_size = 224
        model_kwargs = {}

    elif model_name == "resnet101_lstm":
        batch_size = 3
        img_size = 224
        model_kwargs = {}

    elif model_name == "resnet18_lstm":
        img_size = 224
        model_kwargs = {}

    elif model_name == "c3d":
        n_frames = 16
        n_blocks = 4
        frames_per_block = 4
        model_kwargs = {}

    elif model_name == "resnet3d_101":
        model_kwargs = {}

    elif model_name == "resnet3d_18":
        batch_size = 20
        model_kwargs = {}

    elif model_name == "i3d":
        batch_size = 5
        img_size = 226
        n_frames = 64
        n_blocks = 8
        frames_per_block = 8
        model_kwargs = {
            "dataset_name": dataset_name
        }

    elif model_name == "c3d_block_lstm":
        model_kwargs = {}


    model_kwargs = {
        "model_name": model_name,
        "img_size": img_size,
        "n_labels": n_labels,
        "dropout_prob": dropout_prob,
        "pretrained": pretrained,
        "n_frames": n_frames,
        "n_blocks": n_blocks,
        "frames_per_block": frames_per_block,
        "model_kwargs": model_kwargs
    }
    batch_size =  batch_size * n_gpus

    return model_kwargs, batch_size
