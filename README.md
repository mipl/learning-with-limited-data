# On the Pitfalls of Learning with Limited Data A Facial Expression Recognition Case Study

This repository contains the source code used to create an empirical study in how train deep neural models with limited data to improve the classification process. [TODO: LINK TO PAPER]()


## Models
| Model name     | Model code | Paper |
|----------------|------------|-------|
| VGG16-LSTM       | vgg16_lstm       | TODO: LINK |
| InceptionV3-LSTM | inceptionv3_lstm | TODO: LINK |
| ResNet18-LSTM    | resnet18_lstm    | TODO: LINK |
| ResNet101-LSTM   | resnet101_lstm   | TODO: LINK |
| C3D              | c3d              | TODO: LINK |
| C3D-Block-LSTM   | c3d_Block_lstm   | TODO: LINK |
| I3D              | i3d              | TODO: LINK |
| ResNet3D-18      | resnet3d-18      | TODO: LINK |
| ResNet3D-101     | resnet3d-101     | TODO: LINK |

## Datasets
| Dataset name     | Dataset code | Files | Paper |
|-------|-------|-------|-------|
| CK+ | ck | TODO:FILES | TODO: LINK |
| MMI | mmi | TODO:FILES | TODO: LINK |
| Oulu-CASIA | oulu | TODO:FILES | TODO: LINK |
| MUG | mug | TODO:FILES | TODO: LINK |
| AFEW | afew | TODO:FILES | TODO: LINK |

## Data augmentations
| Data augmentation type | code |
|-------|-------|
| Classic Data augmentation | p |
| Synthetic Data augmentation | s |


## Usage

We create a `makefile` that uses a docker environment with the necessary packages to use the code

The following command is used to start the docker environment
```bash
make run HOST_DATASETS_PATH=/some/path/to/datasets HOST_METADATA_PATH=/another/path/to/metadata GPU=true
```
Arguments `HOST_DATASETS_PATH` and `HOST_METADATA_PATH` are paths to the folders on the local computer where the databases and training metadata are stored.

The `GPU=true` argument is used when the training will be done using gpus.

Then within the docker image it is possible to run three different types of (k-fold experiments:

### Train and test with one datasets
```bash
python3 kfold_main.py -m MODEL_NAME -d DATASET_NAME -e N_EPOCHS -o OPT_NAME -lr LEARNING_RATE -wd WEIGTH_DECAY -p -c PATH_TO_CHECKPOINT -da DATA_AUGMENTATION_TYPES
```
#### Parameter explanation
* `-m MODEL_NAME`: is the name of used model to train (See models table)
* `-d DATASET_NAME`: is the name of used dataset to train (See datasets table)
* `-e N_EPOCHS`: is the number of epochs (int)
* `-o OPT_NAME`: is the name of used optimizer to train (See optimizers table)
* `-lr LEARNING_RATE`: is the learning rate number (float)
* `-wd WEIGTH_DECAY`: is the weight decay number (float)
* `-p`: is the transfer learning parameter, if is present the model is initialized with pretrained weigths to be fine tuned.
* `-c PATH_TO_CHECKPOINT`: is the path to checkpoint file to continue training process
* `-da DATA_AUGMENTATION_TYPES`: is a list separated with `,` of data augmentation applied to this train (See data augmentation table)

### Train with one dataset and test with many others

```bash
python3 cross_dataset_1vall.py -m MODEL_NAME -td TRAIN_DATASET_NAME -ld TEST_DATASET_LIST -e N_EPOCHS -o OPT_NAME -lr LEARNING_RATE -wd WEIGTH_DECAY -p -c PATH_TO_CHECKPOINT -da DATA_AUGMENTATION_TYPES
```
#### Parameter explanation
* `-m MODEL_NAME`: is the name of used model to train (See models table)
* `-td TRAIN_DATASET_NAME `: is the name of used dataset to train (See datasets table)
* `-ld TEST_DATASET_LIST`: is a list separated with `,` of datasets used to test the trained model (See datasets table)
* `-e N_EPOCHS`: is the number of epochs (int)
* `-o OPT_NAME`: is the name of used optimizer to train (See optimizers table)
* `-lr LEARNING_RATE`: is the learning rate number (float)
* `-wd WEIGTH_DECAY`: is the weight decay number (float)
* `-p`: is the transfer learning parameter, if is present the model is initialized with pretrained weigths to be fine tuned.
* `-c PATH_TO_CHECKPOINT`: is the path to checkpoint file to continue training process
* `-da DATA_AUGMENTATION_TYPES`: is a list separated with `,` of data augmentation applied to this train (See data augmentation table)



### Train and test with mix of datasets

```bash
python3 cross_dataset_kfold.py -m MODEL_NAME -ld DATASET_LIST -e N_EPOCHS -o OPT_NAME -lr LEARNING_RATE -wd WEIGTH_DECAY -p -c PATH_TO_CHECKPOINT -da DATA_AUGMENTATION_TYPES
```
#### Parameter explanation
* `-m MODEL_NAME`: is the name of used model to train (See models table)
* `-ld DATASET_LIST`: is a list separated with `,` of datasets used to train and test the model (See datasets table)
* `-e N_EPOCHS`: is the number of epochs (int)
* `-o OPT_NAME`: is the name of used optimizer to train (See optimizers table)
* `-lr LEARNING_RATE`: is the learning rate number (float)
* `-wd WEIGTH_DECAY`: is the weight decay number (float)
* `-p`: is the transfer learning parameter, if is present the model is initialized with pretrained weigths to be fine tuned.
* `-c PATH_TO_CHECKPOINT`: is the path to checkpoint file to continue training process
* `-da DATA_AUGMENTATION_TYPES`: is a list separated with `,` of data augmentation applied to this train (See data augmentation table)




## Citation
If you use this code for your research, please consider citing:

```
  TODO: JSON WITH CITATION HERE
```

## Contact
If you have any problem about our code, feel free to contact
* <m.rodriguezs1990@gmail.com>
* <TODO: ADIN'S EMAIL HERE>
* <TODO: JUAN'S EMAIL HERE>

or describe your problem in Issues.
