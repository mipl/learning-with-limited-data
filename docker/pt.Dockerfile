# Freeze pytorch version from experiments
FROM pytorch/pytorch:1.2-cuda10.0-cudnn7-runtime

MAINTAINER mipl

RUN apt-get -y update -qq && \
    apt-get -y install \
      python3-tk \
      # libglib2.0-0 \
      # libsm6 \
      # libxext6 \
      # libxrender-dev

RUN pip install --no-cache-dir --upgrade pip &&\
    pip install --no-cache-dir \
      community \
      numpy \
      opencv-python \
      scikit-image \
      scipy \
      sklearn \
      tensorboard \
      tensorboardX \
      pandas


# Exposing jupyter and tensorboard
EXPOSE 8888 6006


# Change working dirs
WORKDIR /home
