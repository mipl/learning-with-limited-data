FROM mipl/learning-limited-data:pt

MAINTAINER mipl

# Copy the source
COPY ./*.py /home/
COPY datasets/ /home/datasets/
COPY factories/ /home/factories/
COPY models/ /home/models/
COPY train/ /home/train/
COPY utils/ /home/utils/