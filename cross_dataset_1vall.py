import torch
import sys
import numpy as np
import os

from factories.optimizer import get_optimizer_parameters

from train.train import train, test_epoch

from utils.model_utils import get_dataset_by_model, get_model_params
from utils.dataset_utils import get_dataset_params
from utils.args import get_1vall_args

from utils.experiment import init_experiment, ini_checkpoint_experiment

from factories.optimizer import OptimizerFactory
from factories.model import ModelFactory
from factories.dataset import DatasetFactory

# Ignore warnings
import warnings
warnings.filterwarnings("ignore")



def main():
    args = get_1vall_args()

    torch.manual_seed(0)
    np.random.seed(0)

    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    print("Using:", device)

    # Logs vars
    metadata_path = '../metadata'


    # Train vars
    ini_epoch = 0
    n_epochs = args["n_epochs"]
    lr = args["lr"]
    wd = args["wd"]
    opt_name = args["opt_name"]

    # Network vars
    dropout_prob = args["dropout_prob"]


    # Get optimizer vars
    optimizer_kwargs = get_optimizer_parameters(opt_name, lr=lr, wd=wd)

    # Dataset vars
    dataset_name = args["train_dataset"]

    dataset_params = get_dataset_params(dataset_name, cross=True)

    model_name =  args["model_name"]
    pretrained = args["pretrained"]

    model_params, batch_size = get_model_params(model_name,
                                                dataset_name,
                                                dataset_params["n_labels"],
                                                pretrained=pretrained,
                                                bh=False,
                                                dropout_prob=dropout_prob)

    data_augmentation_kwargs = {
        "type": [],
        "data_augmentation": False,
        "img_size": model_params["img_size"]
    }

    if "c" in args["data_augmentation"]:
        data_augmentation_kwargs["type"].append("c")
    if "p" in args["data_augmentation"]:
        data_augmentation_kwargs["type"].append("p")
        data_augmentation_kwargs["data_augmentation"] = True
        data_augmentation_kwargs["rotation_angle"] = 30
        data_augmentation_kwargs["crop_ratio"] = 0.8
    if  "s" in args["data_augmentation"]:
        data_augmentation_kwargs["type"].append("s")

    dataset_type = get_dataset_by_model(model_name, dataset_name)

    # Dataset loaders
    dataset_path_pattern = '../datasets/{}/kfold_lists/with_crop/groups/cross/{}-{}.list'

    # Load model
    model = ModelFactory.factory(**model_params)
    model.to(device)

    # Create optimizer
    optimizer = OptimizerFactory.factory(model.parameters(), **optimizer_kwargs)

    # Ini experiment
    # test_name = "-".join(args["test_datasets"])
    filename = args["checkpoint_path"]
    if filename != "":
        if os.path.isfile(filename):
            model_dir, log_dir, experiment_id = ini_checkpoint_experiment(filename, model_name, dataset_name, cross=True)

            print("=> loading checkpoint '{}'".format(filename))
            checkpoint = torch.load(filename)
            ini_epoch = checkpoint['epoch']

            if isinstance(model, torch.nn.DataParallel):
                model.module.load_state_dict(checkpoint['state_dict'])
            else:
                model.load_state_dict(checkpoint['state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer'])
            print("=> loaded checkpoint '{}' (epoch {})".format(filename, checkpoint['epoch']))

        else:
            print("=> no checkpoint found at '{}'".format(filename))
            exit()
    else:
        # Init experiment
        model_dir, log_dir, experiment_id = init_experiment(metadata_path, "cross_1vall_train_{}".format(dataset_name), model_name)

    print(model.parameters)

    # Train set
    if  "s" in data_augmentation_kwargs["type"]:
        modes = ["train", "validation", "test", "syn"]
    else:
        modes = ["train", "validation", "test"]

    files = [dataset_path_pattern.format(dataset_name, mode, 0) for mode in modes]

    train_set = DatasetFactory.factory(dataset_type=dataset_type,
                                       csv_file=files,
                                       n_frames=model_params["n_frames"],
                                       n_blocks=model_params["n_blocks"],
                                       frames_per_block=model_params["frames_per_block"],
                                       train=True,
                                       **dataset_params,
                                       **data_augmentation_kwargs)

    trainloader = torch.utils.data.DataLoader(train_set,
                                              batch_size=batch_size,
                                              shuffle=True, num_workers=10)

    train_metrics = train(model=model,
                       optimizer=optimizer,
                       ini_epoch=ini_epoch,
                       n_epochs=n_epochs,
                       device=device,
                       trainloader=trainloader,
                       testloader=None,
                       # testnames=test_dataset_names,
                       model_dir=model_dir,
                       log_dir=log_dir,
                       test=False)
    if "acc" in train_metrics["train"]:
        train_acc = train_metrics["train"]["acc"]
    else:
        train_acc = None
    test_acc = {}

    modes = ["train", "validation", "test"]
    for test_dataset_name in args["test_datasets"]:
        print("Loading test dataset:", test_dataset_name)
        test_dataset_params = get_dataset_params(test_dataset_name, cross=True)

        test_dataset_type = get_dataset_by_model(model_name, test_dataset_name)

        files = [dataset_path_pattern.format(test_dataset_name, mode, 0) for mode in modes]
        test_set = DatasetFactory.factory(dataset_type=test_dataset_type,
                                           csv_file=files,
                                           n_frames=model_params["n_frames"],
                                           n_blocks=model_params["n_blocks"],
                                           frames_per_block=model_params["frames_per_block"],
                                           train=False,
                                           **test_dataset_params,
                                           **data_augmentation_kwargs)

        # test_sets.append(test_set)
        testloader = torch.utils.data.DataLoader(test_set,
                                                 batch_size=batch_size,
                                                 shuffle=True, num_workers=10)

        # testloaders.append(testloader)

        test_metrics = test_epoch(device, testloader, model)
        test_acc[test_dataset_name] = test_metrics["acc"]


if __name__ == "__main__":
    sys.exit(main())
