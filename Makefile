#FUNCTION
define cecho
    @echo "\033[92m$(1)\033[0m"
endef

# rcnn-fer Docker Makefile
REGISTRY_URL=mipl
VERSION=latest
DOCKER_IMAGE=learning-limited-data
DOCKER_CONTAINER_NAME=limited-data


##############################################################################
############################# DOCKER VARS ####################################
##############################################################################
# COMMANDS
DOCKER_COMMAND=docker
NVIDIA_DOCKER_COMMAND=nvidia-docker


#HOST VARS
LOCALHOST_IP=127.0.0.1
HOST_TENSORBOARD_PORT=26006
HOST_NOTEBOOK_PORT=28888

#HOST CPU VARS
HOST_SOURCE_PATH=$(shell pwd)
HOST_DATASETS_PATH=#DATASET FOLDER PATH
HOST_METADATA_PATH=#METADATA FOLDER PATH

#IMAGE VARS
IMAGE_TENSORBOARD_PORT=6006
IMAGE_NOTEBOOK_PORT=8888
IMAGE_SOURCE_PATH=/home/src
IMAGE_DATASETS_PATH=/home/datasets
IMAGE_METADATA_PATH=/home/metadata

# DOCKER vars
EXP_NAME=""
HOSTNAME=$(shell cat /etc/hostname)

# VOLUMES
DOCKER_VOLUMES = --volume=$(HOST_SOURCE_PATH):$(IMAGE_SOURCE_PATH) \
						     --volume=$(HOST_DATASETS_PATH):$(IMAGE_DATASETS_PATH) \
						     --volume=$(HOST_METADATA_PATH):$(IMAGE_METADATA_PATH) \
						     --workdir=$(IMAGE_SOURCE_PATH) \
						     --shm-size 8G


DOCKER_TENSORBOARD_PORTS = -p $(LOCALHOST_IP):$(HOST_TENSORBOARD_PORT):$(IMAGE_TENSORBOARD_PORT)
DOCKER_JUPYTER_PORTS = -p $(LOCALHOST_IP):$(HOST_NOTEBOOK_PORT):$(IMAGE_NOTEBOOK_PORT)

# IF GPU == false --> GPU is disabled
# IF GPU == true --> GPU is enabled
ifeq ($(GPU), true)
	DOCKER_RUN_COMMAND=$(NVIDIA_DOCKER_COMMAND) run -it --rm --userns=host -e HOSTNAME=$(HOSTNAME) --name=$(DOCKER_CONTAINER_NAME)-$(EXP_NAME) $(DOCKER_VOLUMES) $(REGISTRY_URL)/$(DOCKER_IMAGE):$(VERSION)
	DOCKER_RUN_TENSORBOARD_COMMAND=$(DOCKER_COMMAND) run -it --rm --userns=host -e HOSTNAME=$(HOSTNAME) --name=$(DOCKER_CONTAINER_NAME)-$(EXP_NAME) $(DOCKER_TENSORBOARD_PORTS) $(DOCKER_VOLUMES) $(REGISTRY_URL)/$(DOCKER_IMAGE):$(VERSION)
	DOCKER_RUN_JUPYTER_COMMAND=$(NVIDIA_DOCKER_COMMAND) run -it --rm --userns=host -e HOSTNAME=$(HOSTNAME) --name=$(DOCKER_CONTAINER_NAME)-$(EXP_NAME) $(DOCKER_JUPYTER_PORTS) $(DOCKER_VOLUMES) $(REGISTRY_URL)/$(DOCKER_IMAGE):$(VERSION)
else
	DOCKER_RUN_COMMAND=$(DOCKER_COMMAND) run -it --rm --userns=host -e HOSTNAME=$(HOSTNAME) --name=$(DOCKER_CONTAINER_NAME)-$(EXP_NAME)  $(DOCKER_VOLUMES) $(REGISTRY_URL)/$(DOCKER_IMAGE):$(VERSION)
	DOCKER_RUN_TENSORBOARD_COMMAND=$(DOCKER_COMMAND) run -it --rm --userns=host -e HOSTNAME=$(HOSTNAME) --name=$(DOCKER_CONTAINER_NAME)-$(EXP_NAME)  $(DOCKER_TENSORBOARD_PORTS) $(DOCKER_VOLUMES) $(REGISTRY_URL)/$(DOCKER_IMAGE):$(VERSION)
	DOCKER_RUN_JUPYTER_COMMAND=$(DOCKER_COMMAND) run -it --rm --userns=host -e HOSTNAME=$(HOSTNAME) --name=$(DOCKER_CONTAINER_NAME)-$(EXP_NAME)  $(DOCKER_JUPYTER_PORTS) $(DOCKER_VOLUMES) $(REGISTRY_URL)/$(DOCKER_IMAGE):$(VERSION)
endif


# COMMANDS
JUPYTER_COMMAND=jupyter
TENSORBOARD_COMMAND=tensorboard
MKDIR_COMMAND=mkdir
WGET_COMMAND=wget

TENSORBOARD_PATH=$(IMAGE_METADATA_PATH)
# URLs
C3D_URL=http://www.recod.ic.unicamp.br/~mrodriguez/weights/c3d.pickle

setup s:
	@$(MKDIR_COMMAND) -p ./weigths
	@$(WGET_COMMAND) $(C3D_URL) -P ./weigths

run r: docker-print
	@$(DOCKER_RUN_COMMAND)

jupyter j: docker-print
	@$(DOCKER_RUN_JUPYTER_COMMAND)  bash -c "make jupyter CUDA_VISIBLE_DEVICES=$(CUDA_VISIBLE_DEVICES)"; \
	status=$$?

tensorboard t: docker-print
	@$(DOCKER_RUN_TENSORBOARD_COMMAND)  bash -c "make tensorboard TENSORBOARD_PATH=$(TENSORBOARD_PATH)"; \
	status=$$?


#PRIVATE
docker-print psd:
ifeq ($(GPU), true)
	$(call cecho, "[GPU Docker] Running gpu docker image...")
else
	$(call cecho, "[CPU Docker] Running cpu docker image...")
endif

run-jupyter rj:
	$(call cecho, "[Jupyter] Running Jupyter lab")
	@$(EXPORT_COMMAND) CUDA_VISIBLE_DEVICES=$(CUDA_VISIBLE_DEVICES)
	@$(JUPYTER_COMMAND) lab --ip=0.0.0.0 --allow-root

run-tensorboard rtb:
	$(call cecho, "[Tensorboard] Running Tensorboard")
	@$(TENSORBOARD_COMMAND) --logdir=$(TENSORBOARD_PATH) --host 0.0.0.0
